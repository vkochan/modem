;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (xmodem)
  (export
    xmodem?
    make-xmodem
    xmodem-send
    xmodem-recv)
  (import
    (rnrs)
    (hashing crc))

(define-crc crc-16/xmodem 16 #x1021 #x0000 #f #f 0 #x29B1)

(define SOH  #x01)
(define STX  #x02)
(define EOT  #x04)
(define ACK  #x06)
(define XON  #x11)
(define XOFF #x13)
(define NAK  #x15)
(define CAN  #x18)
(define CRC  #x43)

(define-record-type xmodem
  (fields getc putc))

(define xmodem-get-u8
  (case-lambda
    [(xm timeout)
     (define get-bv (xmodem-getc xm))

     (let ([bv (get-bv 1 timeout)])
       (if (bytevector? bv) (bytevector-u8-ref bv 0) bv))]

    [(xm)
     (xmodem-get-u8 xm 1)]))

(define (xmodem-get-bytevector xm n timeout)
  (define get-bv (xmodem-getc xm))
  (get-bv n timeout))

(define xmodem-put-u8
  (case-lambda
    [(xm byte timeout)
     (define put-bv (xmodem-putc xm))
     (put-bv (make-bytevector 1 byte) timeout)]

    [(xm byte)
     (xmodem-put-u8 xm byte 1)]))

(define (xmodem-put-bytevector xm bv timeout)
  (define put-bv (xmodem-putc xm))
  (put-bv bv timeout))

(define (xmodem-abort xm count timeout)
  (let loop ([count count])
    (when (positive? count)
      (xmodem-put-u8 xm CAN timeout)
      (loop (fx- count 1)))))

(define (xmodem-send xm in-port . opts)
  (define pkt-size 128)
  (define timeout 60)
  (define retry 16)

  (let opts-loop ([opts opts])
    (when (not (null? opts))
      (let ([opt (car opts)])
        (cond
          ((eqv? opt 'x-1024)
           (set! pkt-size 1024)) )
      (opts-loop (cdr opts)))))

  (let loop ([done? #f][cancel? #f][err-count 0][crc-mode? #f])
    (cond
      ((>= err-count retry)
       (xmodem-abort xm 2 timeout)
       (error 'xmodem-send "max errors count reached" err-count))

      ((and done? cancel?)
       #f)

      (done?
       (xmodem-send-bytes xm pkt-size in-port crc-mode? retry timeout))

      (else
       (let ([byte (xmodem-get-u8 xm)])
         (cond
           ((eqv? byte NAK)
            (loop #t cancel? err-count #f))
           ((eqv? byte CRC)
            (loop #t cancel? err-count #t))
           ((eqv? byte CAN)
            (loop (or cancel? done?) #t err-count crc-mode?))
           (else (loop done? cancel? (fx+ err-count 1) crc-mode?))))))))

(define (calc-csum bv)
  (fxmod
    (let loop ([i 0][csum 0])
      (if (< i (bytevector-length bv))
        (loop (fx+ i 1) (fx+ csum (bytevector-u8-ref bv i)))
        csum))
    256))

(define (calc-crc bv)
  (crc-16/xmodem bv))

(define (xmodem-send-bytes xm pkt-size in-port crc-mode? retry timeout)
  (define (pad-bytes bv to)
    (cond
      ((eqv? (bytevector-length bv) to)
       bv)
      (else
       (let ([new-bv (make-bytevector to 0)])
         (bytevector-copy! bv 0 new-bv 0 (bytevector-length bv))
         new-bv))))

  (let loop ([seq 1])
    (let ([bv (get-bytevector-n in-port pkt-size)])
      (cond
        ((eof-object? bv)
         (xmodem-send-eot xm retry timeout))
        (else
         (let ([bv (pad-bytes bv pkt-size)])
           (xmodem-send-packet xm seq bv pkt-size crc-mode? retry timeout)
           (loop (fx+ seq 1))))))))

(define (xmodem-send-packet xm seq data pkt-size crc-mode? retry timeout)
  (define crc (if crc-mode? (calc-crc data) (calc-csum data)))
  (define start-byte (if (eqv? pkt-size 128) SOH STX))

  (let loop ([errors 0])
    (when (>= errors retry)
      (xmodem-abort xm 2 timeout)
      (error 'xmodem-send-packet "max errors count reached" errors))
    (xmodem-put-u8 xm start-byte)
    (xmodem-put-u8 xm seq)
    (xmodem-put-u8 xm (fx- #xFF seq))
    (xmodem-put-bytevector xm data 1)
    (if crc-mode?
        (begin
          (xmodem-put-u8 xm (bitwise-arithmetic-shift-right crc 8))
          (xmodem-put-u8 xm (bitwise-and crc #xFF)))
        (xmodem-put-u8 xm crc))
    (let ([byte (xmodem-get-u8 xm timeout)])
      (cond
        ((eqv? byte ACK))

        ((eqv? byte NAK)
         (loop (fx+ errors 1)))
        (else
         (loop (fx+ errors 1)))))))

(define (xmodem-send-eot xm retry timeout)
  (let loop ([errors 0])
    (xmodem-put-u8 xm EOT)
    (let loop ([byte (xmodem-get-u8 xm timeout)])
      (when (not (eqv? byte ACK))
        (if (>= errors retry)
            (error 'xmodem-send-eot "maximum errors count reached" errors)
            (loop (fx+ errors 1)))))))

(define (xmodem-recv xm out-port . opts)
  (define pkt-size 128)
  (define crc-mode? #t)
  (define timeout 60)
  (define retry 16)
  (define byte #f)

  (define (crc-len)
    (fx+ 1 (if crc-mode? 1 0)))

  (define (extract-crc pkt)
    (if crc-mode?
      (let ([crc-hi (bytevector-u8-ref pkt pkt-size)]
            [crc-lo (bytevector-u8-ref pkt (fx+ pkt-size 1))])
        (bitwise-ior
          (bitwise-arithmetic-shift-left crc-hi 8)
          crc-lo))
      (bytevector-u8-ref pkt pkt-size)))

  (define (check-crc data pkt-crc)
    (eqv? pkt-crc
          (if crc-mode?
              (calc-crc data)
              (calc-csum data))))

  (let opts-loop ([opts opts])
    (when (not (null? opts))
      (let ([opt (car opts)])
        (cond
          ((eqv? opt 'no-crc)
           (set! crc-mode? #f)) )
      (opts-loop (cdr opts)))))

  (let init-loop ([errors 0][cancel? #f])
    (when (>= errors retry)
      (xmodem-abort xm 2 timeout)
      (error 'xmodem-recv "maximum erorrs count reached" errors))

    (if (and crc-mode? (< errors (fxdiv retry 2)))
        (xmodem-put-u8 xm CRC)
        (begin
          (set! errors (fx+ errors 1))
          (set! crc-mode? #f)
          (xmodem-put-u8 xm NAK)))

    (set! byte (xmodem-get-u8 xm timeout))
    (cond
      ((or (eqv? byte SOH) (eqv? byte STX)))

      ((eqv? byte CAN)
       (when cancel?
         (error 'xmodem-recv "receiving was canceled"))
       (init-loop errors #t))
      (else (init-loop (fx+ errors 1) cancel?))))

  (let read-loop ([count 0][byte byte][next-seq 1][cancel? #f][data-ready? #f])
    (when (not data-ready?)
      (cond
        ((eqv? byte SOH)
         (set! pkt-size 128)
         (set! data-ready? #t))

        ((eqv? byte STX)
         (set! pkt-size 1024)
         (set! data-ready? #t))

        ((eqv? byte EOT)
         (set! data-ready? #f)
         (xmodem-put-u8 xm ACK))

        ((eqv? byte CAN)
         (if cancel?
             (error 'xmodem-recv "receiving was canceled")
             (read-loop count (xmodem-get-u8 xm) next-seq cancel? data-ready?)))
        (else (error 'xmodem-recv "expected SOH/EOT"))))

    (when data-ready?
      (set! data-ready? #f)

      (let* ([seq (xmodem-get-u8 xm)]
             [seq-ok? (eqv? seq (fx- #xFF (xmodem-get-u8 xm)))])
        (cond
          ((and seq-ok? (eqv? seq next-seq))
           (let ([pkt (xmodem-get-bytevector xm (fx+ pkt-size (crc-len)) timeout)]
                 [data (make-bytevector pkt-size)])
             (bytevector-copy! pkt 0 data 0 pkt-size)
             (if (check-crc data (extract-crc pkt))
               (begin
                 (put-bytevector out-port data)
                 (xmodem-put-u8 xm ACK)
                 (read-loop (fx+ count pkt-size) (xmodem-get-u8 xm) (fx+ next-seq 1) cancel? data-ready?))
               (begin
                 (xmodem-put-u8 xm NAK)
                 (read-loop count (xmodem-get-u8 xm) next-seq cancel? data-ready?)))))

          (else
           (xmodem-get-bytevector xm (fx+ pkt-size (crc-len)) 1)
           (read-loop count (xmodem-get-u8 xm) next-seq cancel? data-ready?)))))
    count))
)
