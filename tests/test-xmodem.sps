#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!chezscheme

(import
  (chezscheme)
  (srfi :64 testing)
  (xmodem))

(print-radix 16)

(define (check-program prog)
  (let ([ret (system (string-append "which " prog " > /dev/null 2>&1"))])
    (and (= ret 0) prog)))

(define rx-prog
  (cond
    ((check-program "rx"))
    ((check-program "lrzsz-rx"))
    (else (error 'rx-prog "rx program not found"))))

(define sx-prog
  (cond
    ((check-program "sx"))
    ((check-program "lrzsz-sx"))
    (else (error 'sx-prog "sx program not found"))))

(define test-file1 "/tmp/xm-test-some-bv-file.out")
(define test-file2 "/tmp/xm-test-some-bv-file2.out")

(define test-bv (string->bytevector "Hello, world of a modems\n" (native-transcoder)))

(define test-bv2 (string->bytevector
"A modulator-demodulator or modem is a computer hardware device that converts data from a digital format into a format suitable for an analog transmission medium such as telephone or radio. A modem transmits data by modulating one or more carrier wave signals to encode digital information, while the receiver demodulates the signal to recreate the original digital information. The goal is to produce a signal that can be transmitted easily and decoded reliably. Modems can be used with almost any means of transmitting analog signals, from light-emitting diodes to radio.

Early modems were devices that used audible sounds suitable for transmission over traditional telephone systems and leased lines. These generally operated at 110 or 300 bits per second (bit/s), and the connection between devices was normally manual, using an attached telephone handset. By the 1970s, higher speeds of 1,200 and 2,400 bit/s for asynchronous dial connections, 4,800 bit/s for synchronous leased line connections and 35 kbit/s for synchronous conditioned leased lines were available. By the 1980s, less expensive 1,200 and 2,400 bit/s dialup modems were being released, and modems working on radio and other systems were available. As device sophistication grew rapidly in the late 1990s, telephone-based modems quickly exhausted the available bandwidth, reaching 56 kbit/s. "
(native-transcoder)))

(test-begin "xmodem-send")
(let-values ([(to-in from-out from-err pid)
                (open-process-ports (string-append rx-prog " --xmodem -b " test-file1))])
  (let ([xm (make-xmodem
              (lambda (count timeout)
                (get-bytevector-n from-out count))

              (lambda (bytes timeout)
                (put-bytevector to-in bytes)
                (flush-output-port to-in)))])
    (xmodem-send xm (open-bytevector-input-port test-bv))))
(test-end)

(test-begin "xmodem-send2")
(let-values ([(to-in from-out from-err pid)
                (open-process-ports (string-append rx-prog " --xmodem -b " test-file2))])
  (let ([xm (make-xmodem
              (lambda (count timeout)
                (get-bytevector-n from-out count))

              (lambda (bytes timeout)
                (put-bytevector to-in bytes)
                (flush-output-port to-in)))])
    (xmodem-send xm (open-bytevector-input-port test-bv2))))
(test-end)

(when (file-exists? test-file1)
  (delete-file test-file1))
(when (file-exists? test-file2)
  (delete-file test-file2))

(define text1
"A blinding cursor pulses in the electric darkness like a
heart coursing with phosphorous light, burning beneath
the derma of black-neon glass.
A PHONE begins to RING, we hear it as though we were
making the call. The cursor continues to throb,
relentlessly patient, until --
")

(call-with-output-file test-file1
  (lambda (p)
    (display text1 p)))

(test-begin "xmodem-recv")
(let-values ([(to-in from-out from-err pid)
                (open-process-ports (string-append sx-prog " --xmodem " test-file1))]
             [(out-port out-bv) (open-bytevector-output-port)])
  (let ([xm (make-xmodem
              (lambda (count timeout)
                (get-bytevector-n from-out count))

              (lambda (bytes timeout)
                (put-bytevector to-in bytes)
                (flush-output-port to-in)))])
    (xmodem-recv xm out-port 'no-crc))
  (display "\n*** Got a message via the modem ***\n\n")
  (display (bytevector->string (out-bv) (native-transcoder)))
  (newline)
)
(test-end)

(when (file-exists? test-file1)
  (delete-file test-file1))
(when (file-exists? test-file2)
  (delete-file test-file2))

(test-begin "xmodem-send/crc")
(let-values ([(to-in from-out from-err pid)
                (open-process-ports (string-append rx-prog " --xmodem -c -b " test-file1))])
  (let ([xm (make-xmodem
              (lambda (count timeout)
                (get-bytevector-n from-out count))

              (lambda (bytes timeout)
                (put-bytevector to-in bytes)
                (flush-output-port to-in)))])
    (xmodem-send xm (open-bytevector-input-port test-bv))))
(test-end)

(when (file-exists? test-file1)
  (delete-file test-file1))
(when (file-exists? test-file2)
  (delete-file test-file2))

(test-begin "xmodem-send2/crc")
(let-values ([(to-in from-out from-err pid)
                (open-process-ports (string-append rx-prog " --xmodem -c -b " test-file2))])
  (let ([xm (make-xmodem
              (lambda (count timeout)
                (get-bytevector-n from-out count))

              (lambda (bytes timeout)
                (put-bytevector to-in bytes)
                (flush-output-port to-in)))])
    (xmodem-send xm (open-bytevector-input-port test-bv2))))
(test-end)

(when (file-exists? test-file1)
  (delete-file test-file1))
(when (file-exists? test-file2)
  (delete-file test-file2))

(define lorem-ipsum
"
Contrary to popular belief, Lorem Ipsum is not simply random text.
It has roots in a piece of classical Latin literature from 45 BC,
making it over 2000 years old. Richard McClintock, a Latin professor
at Hampden-Sydney College in Virginia, looked up one of the more
obscure Latin words, consectetur, from a Lorem Ipsum passage,
and going through the cites of the word in classical literature,
discovered the undoubtable source. Lorem Ipsum comes from sections
1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\"
(The Extremes of Good and Evil) by Cicero, written in 45 BC.
This book is a treatise on the theory of ethics, very popular
during the Renaissance. The first line of Lorem Ipsum,
\"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is
reproduced below for those interested. Sections 1.10.32 and
1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are
also reproduced in their exact original form, accompanied by
English versions from the 1914 translation by H. Rackham.
")

(call-with-output-file test-file1
  (lambda (p)
    (display lorem-ipsum p)))

(test-begin "xmodem-recv/crc")
(let-values ([(to-in from-out from-err pid)
                (open-process-ports (string-append sx-prog " --xmodem " test-file1))]
             [(out-port out-bv) (open-bytevector-output-port)])
  (let ([xm (make-xmodem
              (lambda (count timeout)
                (get-bytevector-n from-out count))

              (lambda (bytes timeout)
                (put-bytevector to-in bytes)
                (flush-output-port to-in)))])
    (xmodem-recv xm out-port))
  (display "\n*** Got a message #2 via the modem ***\n\n")
  (display (bytevector->string (out-bv) (native-transcoder)))
  (newline)
)
(test-end)

(when (file-exists? test-file1)
  (delete-file test-file1))
(when (file-exists? test-file2)
  (delete-file test-file2))

(test-begin "xmodem-send-1k")
(let-values ([(to-in from-out from-err pid)
                (open-process-ports (string-append rx-prog " --xmodem -c -b " test-file2))])
  (let ([xm (make-xmodem
              (lambda (count timeout)
                (get-bytevector-n from-out count))

              (lambda (bytes timeout)
                (put-bytevector to-in bytes)
                (flush-output-port to-in)))])
    (xmodem-send xm (open-bytevector-input-port test-bv2) 'x-1024)))
(test-end)

(when (file-exists? test-file1)
  (delete-file test-file1))
(when (file-exists? test-file2)
  (delete-file test-file2))


(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
